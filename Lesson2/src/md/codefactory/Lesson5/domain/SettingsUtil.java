package md.codefactory.Lesson5.domain;

public class SettingsUtil {

    public static Settings storeUserSettings(String color, int iterrations){

        Settings settings = new Settings();
        settings.setColor(color);
        settings.setIterations(iterrations);

        return settings;
    }

}
