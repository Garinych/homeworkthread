package md.codefactory.dimas.lesson10;

import java.util.*;

public class CollectionsExample {

    public static void main(String[] args) {

        List<String> days = new ArrayList<>();
        // add -> add objects
        days.add("Monday");
        String secondDay = "Tuesday";
        days.add(secondDay);
        days.add(get3thDay());//ne recomeduetsea c ispolizovaniu
        days.add("Thursday");
        days.add("Friday");
        days.add("Saturday");
        days.add("Sunday");

        for (int i = 0; i < days.size(); i++) {
            System.out.println(days.get(i));
        }
        System.out.println("****************");

        days = new LinkedList<>(days);
        days.add("Sunday");
        for (String s : days) {
            System.out.println(s);
        }
        System.out.println("---------------");
        Set<String> daysSet = new HashSet<>(days);
        Iterator<String> iterator = daysSet.iterator();
        while (iterator.hasNext()) {
            String s = iterator.next();
            System.out.println(s);
        }


        daysSet = new TreeSet<>(days);
        System.out.println("************************");
        daysSet.forEach((String day) -> {  //leambda expresion
            System.out.println(day);
        });


    }

    private static String get3thDay() {
        return "Wednesday";
    }
}
