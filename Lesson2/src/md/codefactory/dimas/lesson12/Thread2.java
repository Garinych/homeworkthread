package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class Thread2 implements Runnable {

    private Semaphore two;

    public Thread2(Semaphore two) {
        this.two = two;
    }

    @Override
    public void run() {

            Thread t = Thread.currentThread();

            /*two.acquire();
            System.out.println(t+" acquired s2 " + two);*/

            two.release();
            System.out.println(t + "released s2 "+ two);

    }
}
