package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class DoubleResourceGrabberImpl implements Runnable {

    private Semaphore first;
    private Semaphore second;
    private Semaphore three;

    public DoubleResourceGrabberImpl(Semaphore first, Semaphore second, Semaphore three, Semaphore fore) {
        this.first = first;
        this.second = second;
        this.three = three;
        this.fore = fore;
    }

    private Semaphore fore;

    public DoubleResourceGrabberImpl(Semaphore first, Semaphore second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public void run() {
        try {
            Thread t = Thread.currentThread();
            first.acquire();
            System.out.println(t+" acuire s1 " + first);
           // Thread.sleep(200);
            second.acquire();
            System.out.println(t + "acquired s2 "+ second);
            three.acquire();
            System.out.println(t + "acquired s3 "+ three);
            fore.acquire();
            System.out.println(t + "acquired s4 "+ fore);
            second.release();
            System.out.println(t + "released s2 " + second);
            first.release();
            System.out.println(t + "released s1 " + first);
            three.release();
            System.out.println(t + "released s3 " + three);
            fore.release();
            System.out.println(t + "released s4 " + fore);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
