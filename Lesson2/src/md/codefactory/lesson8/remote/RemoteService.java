package md.codefactory.lesson8.remote;

import org.omg.CORBA.DynAnyPackage.Invalid;

import java.util.Random;

public class RemoteService {

    private Random random = new Random();

    public boolean hasUserDebts(long userId) {
        if(userId<0){
            throw new IllegalArgumentException("User id cannot be negative");
        }
        return random.nextBoolean();
    }
}
