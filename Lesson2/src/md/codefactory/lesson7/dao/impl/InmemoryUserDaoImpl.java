package md.codefactory.lesson7.dao.impl;

import md.codefactory.lesson7.dao.UserDaoIntf;
import md.codefactory.lesson7.domain.User;

import java.util.ArrayList;
import java.util.List;

public class InmemoryUserDaoImpl implements UserDaoIntf {

    private List<User> users = new ArrayList<>();

    @Override
    public void save(User user) {
        users.add(user);

    }

    @Override
    public List<User> findAll() {
        return users;
    }
}
