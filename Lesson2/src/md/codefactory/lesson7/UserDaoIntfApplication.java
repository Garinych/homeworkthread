package md.codefactory.lesson7;


import md.codefactory.lesson7.dao.UserDaoIntf;
import md.codefactory.lesson7.dao.impl.CSVUserDaoImpl;
import md.codefactory.lesson7.dao.impl.InmemoryUserDaoImpl;
import md.codefactory.lesson7.domain.User;


import javax.swing.*;

import java.io.FileNotFoundException;

import java.io.IOException;

import java.util.ArrayList;

import java.util.List;


public class UserDaoIntfApplication {

    public static void main(String[] args) {


//        InmemoryUserDaoImpl userDao = new InmemoryUserDaoImpl();
        //UserDaoIntf userDaoIntf = new InmemoryUserDaoImpl();
        UserDaoIntf inmemoryUserDao = new InmemoryUserDaoImpl();

        //create new users
        List<User> users = new ArrayList<>();
        users.add(new User(1L, "John", "Doe"));
        users.add(new User(2L, "Michel", "Douglas"));
        users.add(new User(3L, " ", "Born"));


//        System.out.println("Show in memory - ");
//        saveInMemory(inmemoryUserDao, users);
//        showInMemory(inmemoryUserDao);
//        System.out.println("***************");
//        System.out.println("Show in file - ");
//        String insertFileName = null;
//        saveToFile(insertFileName, users );
//        showInFile();

        String userInput;

        String[] outputOptions = new String[]{"InMemory", "InFile"};
        String[] fileOptions = new String[]{"Default", "Other"};
        Object userOption = JOptionPane.showInputDialog(
                null,
                "Select an output option",
                "Output option",
                JOptionPane.QUESTION_MESSAGE,
                null,
                outputOptions,
                null

        );

        userInput = (String) userOption;

        //validate input

        if (userInput.equals("InMemory")) {
            saveInMemory(inmemoryUserDao, users);
            showInMemory(inmemoryUserDao);

        }

        if (userInput.equals("InFile")) {

            Object userInputOption = JOptionPane.showInputDialog(

                    null,
                    "Select an file",
                    "File option",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    fileOptions,
                    null

            );

            userInput = (String) userInputOption;

            if (userInput.equals("Default")) {
                showInFile(null);
            }

            if (userInput.equals("Other")) {
                userInput = JOptionPane.showInputDialog("Enter file name ");
                saveToFile(userInput, users);
                showInFile(userInput);

            }
        }

    }


    public static void showInMemory(UserDaoIntf inmemoryUserDao) {

        try {
            List<User> users = inmemoryUserDao.findAll();
            for (int i = 0; i < users.size(); i++) {
                System.out.println(users.get(i).getId() + ", "

                        + users.get(i).getFirstName() + ", "

                        + users.get(i).getLastName()
                );
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }


    public static void showInFile(String fileName) {

        try {
            List<User> users = new CSVUserDaoImpl(fileName).findAll();
            StringBuffer output = new StringBuffer();
            for (User user : users) {
                output.append("\"" + "User is : " + user.getId() + ", " + user.getFirstName() + ", " + user.getLastName() + "\"\n");
                //System.out.println("User is : " + user.getId() + ", " + user.getFirstName() + ", " + user.getLastName());
                //System.out.println(output.toString());
            }

            JOptionPane.showMessageDialog(null, output.toString());

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }


    public static void saveInMemory(UserDaoIntf inmemoryUserDao, List<User> users) {

        for (User user : users) {
            try {
                inmemoryUserDao.save(user);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }


    public static void saveToFile(String fileName, List<User> users) {

        UserDaoIntf csvUserDao = new CSVUserDaoImpl(fileName);
        for (User user : users) {
            try {
                csvUserDao.save(user);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
