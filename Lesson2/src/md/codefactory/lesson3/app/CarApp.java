package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Car;
import md.codefactory.lesson3.domain.Engine;

public class CarApp {

    public static void main(String[] args) {

        //create a new Car
        Car c16 = new Car();
        //set the color
        c16.color = "darkGreen";
        //set engine
        //first create engine
        Engine e = new Engine();
        e.capacity = 6000;
        e.fuelType = "gasoline";

        c16.engine = e;

        //show all attributes:
        //show car color
        System.out.println("Car color is " + c16.color);
        //show the capacity of the engine for this car
        System.out.println("Capacity is " + c16.engine.capacity);
        System.out.println("Fuell Type is " + c16.engine.fuelType);
    }
}
