package md.codefactory.dimas.lesson10;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapExercise {

    public static void main(String[] args) {

        String text = getText();

        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < text.length(); i++) {
            //System.out.println("on index " + i + " the char is " + text.charAt(i));

            char c = text.charAt(i);
            //ask map if it contains this char
            /*boolean hasChar = map.containsKey(c);
            if (hasChar) {
                //get current value
                int repeats = map.get(c);
                repeats++;
                map.put(c, repeats);
            } else {
                //first occurrence(vstrecea)
                map.put(c, 1);
            }*/
            int value = map.getOrDefault(c, 0);
            map.put(c,++value);
        }

        Set<Character> chars = map.keySet();
        chars.forEach(character -> {
            String msgFormat = "char %s repeats %d times in our text\n";

            Integer repeats = map.get(character);
            System.out.printf(msgFormat, character, repeats);//ctrl + alt +v
        });
    }

    private static String getText() {
        //String getText = JOptionPane.showInputDialog("Enter some text");
        return JOptionPane.showInputDialog("Enter your text here");
    }
}
