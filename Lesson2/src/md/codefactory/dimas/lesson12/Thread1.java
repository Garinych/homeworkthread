package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class Thread1 implements Runnable {

    private Semaphore first;
    private Semaphore second;

    public Thread1(Semaphore first) {
        this.first = first;
    }


    /*public Thread1(Semaphore first, Semaphore second) {
        this.first = first;
        this.second = second;
    }*/

    @Override
    public void run() {
        try {
        Thread t = Thread.currentThread();
        first.release();
        System.out.println(t + "released s1 " + first);

            Thread.sleep(200);

       // second.release();
        //System.out.println(t + "released s2 " + second);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
