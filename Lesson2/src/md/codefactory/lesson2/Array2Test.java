package md.codefactory.lesson2;

public class Array2Test {

    public static void main(String[] args) {

        String[] days = new String[7];
        days[0] = "Monday";
        days[1] = "Tuesday";
        String t = "Wednesday";
        days[2] = t;
        t = new String("Thursday"); // new location of memory
        days[3] = t;
        days[4] =  new String("Friday");
        days[5] = "Saturday";
        days[6] = "Sunday";

        System.out.println("array size is "+ days.length);
        int i = 0;
        for (String s: days) {
            System.out.println("On index "+i+" we have "+s);
            i++;
        }

    }
}
