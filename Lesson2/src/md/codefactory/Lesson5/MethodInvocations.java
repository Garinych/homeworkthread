package md.codefactory.Lesson5;

public class MethodInvocations {

    public static void main(String[] args) {

        UtilMethods.sayHallow();

        UtilMethods.sayHellow("Denis");

        UtilMethods.sayHellow("Jora", 2);

    }
}
