package md.codefactory.lesson3.domain;

public class DB {

    private String host;
    private Long port;
    private String name;

    private static volatile DB db;

    private DB() {
    }

    public DB(String host, Long port, String name) {
        this.host = host;
        this.port = port;
        this.name = name;
    }

    public DB getDBConection(){
        if(this.db == null){
            return new DB();
        } else {
            return db;
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
