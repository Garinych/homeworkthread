package md.codefactory.lesson6;

import md.codefactory.lesson6.utils.BankAccountUtil;
import md.codefactory.lesson6.utils.PinUtil;

import javax.swing.*;
import java.awt.*;

public class ATMProgram {

    public static void main(String[] args) {
        System.out.println("Welcome to ATM");
        int numberOfTries = 0;
        String userInput;
        while (true) {
            userInput = JOptionPane.showInputDialog("Enter your PIN");

            //convert String to INT
            int pin = Integer.parseInt(userInput);

            boolean isCorrectPin = PinUtil.validatePin(pin);

            //if(isCorrectPin == false)
            if(!isCorrectPin){ // WRONG PIN, isCorrectPin is not true
                // how many trise ?
                if(numberOfTries == 2){
                    showErrorMessage();
                    System.exit(-1);

                }
                numberOfTries++;
                JOptionPane.showInputDialog(null,"Sorry Wrong PIN\n Attempts left: " + (3-numberOfTries));
            } else {
                //get out from while
                break;
            }
        }
        // here we have a correct PIN, ask for money userInput
        String[] inputOptions = new String[]{"100", "500", "1000", "2000", "Other"};
        Object userOption = JOptionPane.showInputDialog(null, "Select a amount", "Amount chooser", JOptionPane.QUESTION_MESSAGE, null, inputOptions, inputOptions[0]);
        userInput = (String) userOption;
        //validate for input
        if(userInput.equals("Other")){
            userInput = JOptionPane.showInputDialog("Enter custom amount");
        }
        // convert user input to int
        int amount = Integer.parseInt(userInput);

        boolean isValidSum = BankAccountUtil.validateSum(amount);
        if(isValidSum){
            JOptionPane.showMessageDialog(null, "Take your money, total " + amount);
        } else {
            JOptionPane.showMessageDialog(null, "Insufficient Funds");
        }

    }

    private static void showErrorMessage() {
        JOptionPane.showMessageDialog(null,"No more attempts left", "Attention" ,JOptionPane.ERROR_MESSAGE);
    }
}
