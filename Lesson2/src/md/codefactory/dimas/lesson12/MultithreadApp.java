package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class MultithreadApp {

    public static void main(String[] args) {

        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(1);


        Thread t = new Thread(new DoubleResourceGrabberImpl(s1, s2));
        //now reverse them ... here comes trouble! Dead lock
        Thread t2 = new Thread(new DoubleResourceGrabberImpl(s1, s2));



        t.start();
        t2.start();


        /*try {
            t.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

    }
}
