package md.codefactory.dimas.lesson12;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class FutureApp {

    public static void main(String[] args) {

        //CompletableFuture functions
        CompletableFuture<String> welcomeText = CompletableFuture.supplyAsync(() -> {

            try {

                TimeUnit.SECONDS.sleep(1);

                System.out.println("First method execution");

            } catch (InterruptedException e) {

                throw new IllegalStateException(e);

            }

            return "Interns";

        }).thenApply(name -> {

            return "Hello " + name;

        }).thenApply(greeting -> {

            return greeting + ", welcome to the CF lessons";

        }).exceptionally(ex -> {

            System.out.println("Oops! We have an exception - " + ex.getMessage());

            return "Unknown!";

        });


        futureResult(welcomeText);

        futureResult(welcomeText);


        //Composition

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> "Hello")

                .thenCompose(s -> CompletableFuture.supplyAsync(() -> s + " World"));


        futureResult(completableFuture);


        CompletableFuture<String> combineFuture = CompletableFuture.supplyAsync(() -> "Hello")

                .thenCombine(CompletableFuture.supplyAsync(

                        () -> " World"), (s1, s2) -> s1 + s2);

        int n = 9;
        CompletableFuture<String> result = factorial(n)
                .thenCombine(log(new Double(n)), (f, l) -> f + l)
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result = " + res);
        futureResult(result);


        CompletableFuture<String> result1 = factorial(n)
                .thenCompose(fk -> log(fk))
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result = " + res);
        futureResult(result1);
    }

    private static CompletableFuture<Double> log(Double d){
        return CompletableFuture.supplyAsync(() -> Math.log10(d));
    }

    private static CompletableFuture<Double> log2(int d){

        return CompletableFuture.supplyAsync(() -> Math.log(d));
    }

    private static int recursiveFactorial(int n){
        if(n <= 1) return 1;
        return n  * recursiveFactorial(n-1);
    }

    private static CompletableFuture<Double> factorial(int n){
        return CompletableFuture.supplyAsync(() -> new Double(recursiveFactorial(n)));
    }

    private static void futureResult(CompletableFuture<String> text) {
        try {
            System.out.println(text.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
