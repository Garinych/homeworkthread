package md.codefactory.Lesson5.domain;

public class Settings {

    private String color;
    private int iterations;

    public Settings() {
    }

    public Settings(String color, int iterations) {
        this.color = color;
        this.iterations = iterations;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "color='" + color + '\'' +
                ", iterations=" + iterations +
                '}';
    }
}
