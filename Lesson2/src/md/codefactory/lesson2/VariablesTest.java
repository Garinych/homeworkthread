package md.codefactory.lesson2;

public class VariablesTest {
    public static void main(String[] args) {
        //create int variable
        int x;
        // initialize x varoable with a value
        x = 10;

        System.out.println(x);

        char c ='a';

        System.out.println(c);
        // convert char to int

        x = c;
        System.out.println(x);

        int z = 65533;
        c =(char) z;

        System.out.println(c);
    }

}
