package md.codefactory.lesson3.domain;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Animal {

    private String voice;
    private int age;
    private String name;
    private Array[] child;

    private static volatile Animal animal;

    public Animal() {
    }

    public Animal(String voice, int age, String name, Array[] child) {
        this.voice = voice;
        this.age = age;
        this.name = name;
        this.child = child;
    }

    public Animal getAnimalVoice(String voice, int age, String name, Array[] child){
        if(voice == "ham"){
            return new Dog(age, name, child, 0);
        } else {
            return new Animal(voice, age, name, child);
        }
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Array[] getChild() {
        return child;
    }

    public void setChild(Array[] child) {
        this.child = child;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "voice='" + voice + '\'' +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", child=" + Arrays.toString(child) +
                '}';
    }
}
