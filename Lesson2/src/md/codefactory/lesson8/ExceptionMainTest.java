package md.codefactory.lesson8;

import jdk.nashorn.internal.scripts.JO;
import md.codefactory.lesson7.domain.User;
import md.codefactory.lesson8.dto.UserDataDto;
import md.codefactory.lesson8.exceptions.UserNotFoundException;
import md.codefactory.lesson8.service.UserService;

import javax.swing.*;
import java.io.IOException;

public class ExceptionMainTest {
    public static void main(String[] args) {

        UserService userService = new UserService();

        String userInput = JOptionPane.showInputDialog("Enter User ID pleas");
        long userId = Long.parseLong(userInput);

        try {
            UserDataDto userData = userService.getFullData(userId);
            displayUserData(userData);
        } /*catch (IOException e) {
            String errorMessage = "Sorry, file cannot be opened\n" + "Contact administrator and show this mesage" + e.getMessage();
            showErrorMessage(errorMessage);
        } catch (UserNotFoundException e) {
            String errorMessage = e.getMessage();
            showErrorMessage(errorMessage);
        } */catch (Exception e){
            String message = "An unexpected exception occurred"; //occurred - neojidannaea
            showErrorMessage(message);
        }
    }

    private static void showErrorMessage(String errorMessage) {
        JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
    }

    private static void displayUserData(UserDataDto userData) {
        JOptionPane.showMessageDialog(null, userData.toString(), "User Data", JOptionPane.INFORMATION_MESSAGE);
    }
}
