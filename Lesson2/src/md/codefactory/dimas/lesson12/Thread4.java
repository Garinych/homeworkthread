package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class Thread4 implements Runnable {

    private Semaphore phore;

    public Thread4(Semaphore phore) {
        this.phore = phore;
    }

    @Override
    public void run() {
        try {
            Thread t = Thread.currentThread();

            Thread.sleep(10);
            phore.acquire();
            System.out.println(t+" acquired s4 " + phore);

            phore.release();
            System.out.println(t + "released s4 "+ phore);

        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
