package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Display;
import md.codefactory.lesson3.domain.Laptop;
import md.codefactory.lesson3.domain.Type;

public class LaptopApp {

    public static void main(String[] args) {

        //Create new Laptop
        Laptop lt1 = new Laptop();
        //Set laptop properties
        lt1.model = "Samsung L390";
        lt1.year = 2017;
        //Create new Display
        Display d_lt1 = new Display();
        //Set display properties
        d_lt1.resolution = 1920;
        d_lt1.size = 21.5F;
        //Set display Type
        d_lt1.type = Type.OLED;

        lt1.display = d_lt1;

        //Show lt1 properties
        System.out.println("Model is " + lt1.model);
        System.out.println("Year is " + lt1.year);
        System.out.println("Display resolution is " + d_lt1.resolution);
        System.out.println("Display size is " + d_lt1.size);
        System.out.println("Display type is " + d_lt1.type);



    }
}
