package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class Multithread2App {

    public static void main(String[] args) {

        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(0);
        Semaphore s3 = new Semaphore(1);
        Semaphore s4 = new Semaphore(1);


        Thread t1 = new Thread(new Thread1(s1));
        Thread t2 = new Thread(new Thread2(s2));
        Thread t3 = new Thread(new Thread3(s3));
        Thread t4 = new Thread(new Thread4(s4));

        t1.start();
        t2.start();
        t3.start();
        t4.start();


        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
