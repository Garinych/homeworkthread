package md.codefactory.lesson8.exceptions;

import java.time.LocalDateTime;

public class UserNotFoundException extends Exception {
    private LocalDateTime time;

    public UserNotFoundException(String message) {
        super(message);
        this.time = LocalDateTime.now();
    }

    public LocalDateTime getTime() {
        return time;
    }

}
