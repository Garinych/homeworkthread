package md.codefactory.dimas.lesson10;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapExample {

    public static void main(String[] args) {

        Map<String,String> map = new HashMap<>();
        map.put("bos001","Mercedes 600");
        map.put("xxx1", "BMW X6");

        System.out.println("Total elements " +map.size());

        //rad from map
        String car = map.get("bos001");
        System.out.println(car);
        System.out.println(map.get("-1"));

        String s = map.getOrDefault("-1", "Sorry, no car!");
        System.out.println(s);

        //iterate on all map objects
        Set<String> keys = map.keySet();
        keys.forEach(key -> {
            String messageFormat = "On key %s the value is %s\n";
            System.out.printf(messageFormat, key, map.get(key));
        });
        System.out.println(map.containsKey("bos001"));
        System.out.println(map.containsKey("bos002"));
    }
}
