package md.codefactory.Lesson5;

import java.time.LocalDate;

public class DateUtil {

    public static int getCurrentDate(){
        LocalDate now = LocalDate.now();
        int todayDate = now.getDayOfMonth();
        return todayDate;
    }
}
