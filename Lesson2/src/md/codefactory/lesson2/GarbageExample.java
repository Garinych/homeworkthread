package md.codefactory.lesson2;

public class GarbageExample {

    public static void main(String[] args) {

        String s = "java";

        String s2 = "c#";

        s = s2;
        System.out.println(s);
        System.out.println(s2);
        System.out.println(s == s2);

    }
}
