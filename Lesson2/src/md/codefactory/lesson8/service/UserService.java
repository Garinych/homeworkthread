package md.codefactory.lesson8.service;

import md.codefactory.lesson7.dao.UserDaoIntf;
import md.codefactory.lesson7.dao.impl.CSVUserDaoImpl;
import md.codefactory.lesson7.domain.User;
import md.codefactory.lesson8.dto.UserDataDto;
import md.codefactory.lesson8.exceptions.UserNotFoundException;
import md.codefactory.lesson8.mappers.UserMapper;
import md.codefactory.lesson8.remote.RemoteService;

import java.io.IOException;
import java.util.List;

public class UserService {

    private UserDaoIntf userDaoIntf = new CSVUserDaoImpl();
    private RemoteService remoteService = new RemoteService();

    public UserDataDto getFullData(long userId) throws IOException, UserNotFoundException {
        //get local user info (User Dao)
        User user = getUserFromFile(userId);
        //get remote data (has debts)
        boolean hasDebts = remoteService.hasUserDebts(userId);

        UserDataDto dto = UserMapper.from(user);
        dto.setHasDebts(hasDebts);
        return dto;

    }

    private User getUserFromFile(long userId) throws IOException, UserNotFoundException {
        List<User> users = userDaoIntf.findAll();
        for (User user : users){
            if(user.getId() == userId){
                return user;
            }
        }
        throw new UserNotFoundException("User with ID " + userId + " was not found in our database");
    }

}
