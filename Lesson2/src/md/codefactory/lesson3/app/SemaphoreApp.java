package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Semaphore;

public class SemaphoreApp {

    public static void main(String[] args) {

        //declare a new Semaphore Object
        Semaphore s1;
        //initialize s1 Object
        s1 = new Semaphore();
        //show all properties
        System.out.println(s1.color);
        System.out.println(s1.hasTimer);

        System.out.println("Chanege s1 properties! ");

        s1.color = "Red ";

        System.out.println(s1.color);
        System.out.println(s1.hasTimer);

        //declare a mew Semaphore Object
        Semaphore s2 = new Semaphore();

        s2.color = "Green";
        s2.hasTimer = true;

        System.out.println("Show s2 properties - \n");

        System.out.println(s2.color);
        System.out.println(s2.hasTimer);
    }
}
