package md.codefactory.lesson3.domain;

public class Laptop {

    public String model;
    public int year;
    public Display display;
}
