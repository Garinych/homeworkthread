package md.codefactory.lesson8.mappers;

import md.codefactory.lesson7.domain.User;
import md.codefactory.lesson8.dto.UserDataDto;

public class UserMapper {
    public static UserDataDto from(User user) {
        UserDataDto dto = new UserDataDto();
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        return  dto;
    }
}
