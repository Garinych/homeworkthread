package md.codefactory.lesson3.domain;

public class User {

    private String name;
    private String email;
    private Long id;

    private static volatile User user; // volatile - ?

    public User(){

    }

    public User(String name, String email, Long id) {
        this.name = name;
        this.email = email;
        this.id = id;
    }

    public User getUniqueUser(){
        if(this.user == null){
            return new User();
        }else{
            return user;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", id=" + id +
                '}';
    }
}
