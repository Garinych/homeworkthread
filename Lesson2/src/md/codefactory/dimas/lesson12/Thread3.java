package md.codefactory.dimas.lesson12;

import java.util.concurrent.Semaphore;

public class Thread3 implements Runnable {

    private Semaphore three;

    public Thread3(Semaphore three) {
        this.three = three;
    }

    @Override
    public void run() {
        try {
            Thread t = Thread.currentThread();

            Thread.sleep(1);
            three.acquire();
            System.out.println(t+" acquired s3 " + three);

            three.release();
            System.out.println(t + "released s3 "+ three);

        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
