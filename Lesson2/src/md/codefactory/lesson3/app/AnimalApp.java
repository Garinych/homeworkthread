package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Animal;
import md.codefactory.lesson3.domain.Dog;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AnimalApp {

    public static void main(String[] args) {

        // create new Animal

        Animal cat = new Animal();

        Animal catChild1 = new Animal("Meaw", 1, "Murzik", new Array[0]);

        ArrayList<Animal> catChild = new ArrayList<Animal>();
        catChild.add(catChild1);


        cat.setVoice("Meaw!");
        cat.setAge(5);
        cat.setName("Kity");
        cat.setChild(new Array[1]);

        System.out.println(cat);
        System.out.println(catChild1);

        Dog dog1 = new Dog(4, "Rex", new Array[0], 25 );

//        dog1.getVoice();
//        dog1.setAge(4);
//        dog1.setName("Rex");
//        dog1.setChild(new Array[0]);
//        dog1.setSpeed(25);

        System.out.println(dog1);

       // System.out.println(dog1.getAnimalVoice(4, "Rex", new Array[0], 25));



    }
}
