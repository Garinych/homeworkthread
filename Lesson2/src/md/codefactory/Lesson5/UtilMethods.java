package md.codefactory.Lesson5;

import md.codefactory.Lesson5.domain.Settings;

public class UtilMethods {

    public static void sayHallow(){
        System.out.println("Hellow");
    }

    public static void sayHellow(String name){
        System.out.println("Hellow " + name);
    }

    public static void sayHellow(String name, int iterations){

        for (int i=0; i<iterations; i++){
            sayHellow(name);
        }
    }

    public static void sayHellow(Settings settings){
        for (int i = 0; i<settings.getIterations(); i++){
            System.out.println(settings.getColor() + " hellow");
        }
    }

    public static void  sayHellow(Settings settings, String text){
        for (int i = 0; i<settings.getIterations(); i++){
            System.out.println(settings.getColor() + text);
        }
    }

    public static void main(String[] args) {

        sayHallow();

    }
}
