package md.codefactory.lesson7;

import md.codefactory.lesson7.dao.impl.CSVUserDaoImpl;
import md.codefactory.lesson7.domain.User;

import java.io.IOException;
import java.util.List;

public class UserFileDaoTest {

    public static void main(String[] args) {
        try {
            List<User> users = new CSVUserDaoImpl().findAll();
            for (User user: users) {
                System.out.println();
            }

        }catch (IOException e){
            System.err.println(e.getMessage());
        }

    }
}
