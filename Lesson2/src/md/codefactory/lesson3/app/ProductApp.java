package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Product;

import javax.swing.*;

public class ProductApp {

    public static void main(String[] args) {

        Product p = new Product();

        String productName = JOptionPane.showInputDialog("Enter product name, please ");
        p.name = productName;

        String productPrice  =  JOptionPane.showInputDialog("Enter product price, please ");

        //convert productPrice to double
        double price = Double.parseDouble(productPrice);
        p.price = price;
                                           //You entered the name "TV" with price 2000
        String message = String.format("You entered the name %s with price %.2f", p.name, p.price );

        System.out.println(message);
    }
}
