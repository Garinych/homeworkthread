package md.codefactory.lesson3.domain;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Dog extends Animal{

    private int speed;

    private static String voice = "ham";

    public Dog(){
    }

    public Dog( int age, String name, Array[] child, int speed) {
        super(voice, age, name, child);
        this.speed = speed;
    }

    public Dog(int speed) {
        this.speed = speed;
    }


    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "voice='" + voice + '\'' +
                ", age=" + super.getAge() +
                ", name='" + super.getName() + '\'' +
                ", child=" + Arrays.toString(super.getChild()) +
                ", speed=" + speed +
                '}';
    }
}
