package md.codefactory.lesson7.dao.impl;

import md.codefactory.lesson7.dao.UserDaoIntf;
import md.codefactory.lesson7.domain.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVUserDaoImpl implements UserDaoIntf {

    private final String FILE_NAME = "Users.csv";
    private String userFile;
    private List<User> users = new ArrayList<>();

    public CSVUserDaoImpl() {
    }

    public CSVUserDaoImpl(String userFile) {
        this.userFile = userFile;
    }

    @Override
    public void save(User user) throws FileNotFoundException {

        //first add it in memory
        users.add(user);
        //save list to file
        saveToFile();

    }

    /*
    for example user : {2 , John, Doe} should be converted to string
    */
    private void saveToFile() throws FileNotFoundException {
        //convert all objects to string
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < users.size(); i++) {
            User currentUser = users.get(i);
            stringBuilder.append(currentUser.getId());
            stringBuilder.append(',');
            stringBuilder.append(currentUser.getFirstName());
            stringBuilder.append(',');
            stringBuilder.append(currentUser.getLastName());
            if (i < users.size() - 1) {
                //do no add new line on last object
                stringBuilder.append('\n'); // new line
            }
        }
        PrintWriter writer;
        if(this.userFile!=null) {
            writer = new PrintWriter(userFile);
        }else {
            writer = new PrintWriter(FILE_NAME);
        }
        writer.write(stringBuilder.toString());
        writer.flush();

    }

    @Override
    public List<User> findAll() throws IOException {
        List<User> fileUsers = new ArrayList<>();

        String source;
        if(userFile!=null){
            source = userFile;
        } else {
            source = FILE_NAME;
        }

        BufferedReader reader = new BufferedReader(new FileReader(source));

        String line;
        while ((line = reader.readLine()) != null) {
            //proccess the line
            //split by comma
            String[] words = line.split(",");
            // ["1","John","Doe"]
            //User user = new User();
            Long userId = Long.parseLong(words[0]);
            String firstName = words[1];
            String lastName = words[2];
            User user = new User(userId, firstName, lastName);
            fileUsers.add(user);

        }

        return fileUsers;
    }
}
