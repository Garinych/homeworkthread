package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Employee;

public class EmployeeApp {

    public static void main(String[] args) {

        //create an employee object
        Employee e = new Employee("John Doe",-5000);
        e.setName("John Doe");
        e.setSalary(-5000);

        System.out.println(e);
    }
}
