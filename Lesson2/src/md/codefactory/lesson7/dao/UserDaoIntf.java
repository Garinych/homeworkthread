package md.codefactory.lesson7.dao;

import md.codefactory.lesson7.domain.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface UserDaoIntf { // DAO - data access object

    void save(User user) throws FileNotFoundException;

    List<User> findAll() throws IOException;

}
