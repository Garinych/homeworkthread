package md.codefactory.Lesson5;

import md.codefactory.Lesson5.domain.Settings;
import md.codefactory.Lesson5.domain.SettingsUtil;
import md.codefactory.Lesson5.domain.UserColor;

import javax.swing.*;

public class UserInputProgram {

    public static void main(String[] args) {

        String color;
        int iterations;

        //String[] options = new String[]{"\u001B[31m", "\u001B[32m"};
        UserColor[] options =  new UserColor[]{
                //1
                new UserColor("Red", "\u001B[31m"),
                //2
                new UserColor("Green", "\u001B[32m")
        };
        Object userInput = JOptionPane.showInputDialog(null, "Select a color", "Color chooser", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
        //cast userInput to string
        //color = (UserColor) userInput;

        UserColor selectedOption = (UserColor) userInput;
        color = selectedOption.getColorCode();

        String numberInput = (String) JOptionPane.showInputDialog("How many iterations");
        iterations = Integer.parseInt(numberInput);

        Settings settings = SettingsUtil.storeUserSettings(color, iterations);

        //UtilMethods.sayHellow(settings);

        String text = JOptionPane.showInputDialog("Please insert your text");
        UtilMethods.sayHellow(settings, text);
    }
}
