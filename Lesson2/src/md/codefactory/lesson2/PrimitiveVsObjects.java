package md.codefactory.lesson2;

import javax.swing.*;

public class PrimitiveVsObjects {

    public static void main(String[] args) {

        double d = 10.5D;

        Double dObject = 10.5D;

        System.out.println(d==dObject);

        System.out.println(dObject.intValue());
        System.out.println(dObject.isInfinite());


        String userInput = JOptionPane.showInputDialog("Your salary?");

        System.out.println(userInput);

        //double salary = userInput; //Fatal error

        double salary = Double.parseDouble(userInput);// short way
        double s2 = Double.valueOf(userInput); // long way
        System.out.println(salary);
    }
}
